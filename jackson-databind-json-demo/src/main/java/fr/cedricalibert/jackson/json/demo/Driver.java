package fr.cedricalibert.jackson.json.demo;

import java.io.File;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Driver {

	public static void main(String[] args) {
		try {
			//create object mapper
			ObjectMapper mapper = new ObjectMapper();
			//read the json file and map to JSON pojo
			Student student = mapper.readValue(
					new File("data/sample-full.json"), Student.class
				);
			//print first and last name
			System.out.println("student firstname : "+student.getFirstName());
			System.out.println("student lastname : "+student.getLastName());
			
			//print out address: street and city
			Address address = student.getAddress();
			
			System.out.println("address street : "+address.getStreet());
			System.out.println("address city : "+address.getCity());
			
			//print out languages
			for (String lang : student.getLanguages()) {
				System.out.println(lang);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
